class Field {
  constructor(x, y, height) {
    this.x = x;
    this.y = y;
    this.height = height;
  }
}

function compareHeight(a, b) {
  if (a.height < b.height) {
    return -1;
  }
  else if (a.height > b.height) {
    return 1;
  }
  else { return 0; }
}

function isInsideMap(i, j, map) {
  return (i >= 0
    && i < map.length
    && j >= 0
    && j < map[0].length);
}

function createMatrixWithOptionalPath(map, row, col) {


  let vertexMatrix = new Array(row);
  for (let i = 0; i < row; i++) {
    vertexMatrix[i] = new Array(col);
    for (let j = 0; j < col; j++) {
      vertexMatrix[i][j] = Number.MAX_VALUE;
    }
  }

  vertexMatrix[0][0] = map[0][0];

  let queue = new Array();

  queue.push(new Field(0, 0, vertexMatrix[0][0]));
  queue.sort(compareHeight);

  const directionX = [-1, 0, 1, 0];
  const directionY = [0, 1, 0, -1];

  while (!(queue.length === 0)) {
    let current = queue.shift();

    for (let i = 0; i < 4; i++) {
      let rows = current.x + directionX[i];
      let cols = current.y + directionY[i];

      if (isInsideMap(rows, cols, map)) {
        if (vertexMatrix[rows][cols] >
          vertexMatrix[current.x][current.y] + map[rows][cols]) {

          if (vertexMatrix[rows][cols] !== Number.MAX_VALUE) {
            let adjusmentValue = new Field(rows, cols, vertexMatrix[rows][cols]);
            queue.shift(adjusmentValue);
          }

          vertexMatrix[rows][cols] = vertexMatrix[current.x][current.y] +
            map[rows][cols];

          queue.push(new Field(rows, cols, vertexMatrix[rows][cols]));
          queue.sort(compareHeight);
        }
      }
    }
  }
  return vertexMatrix;
}

function createArrayWithVertexPointsOfOptionalPath(map, row, col) {

  let currentRow = row - 1,
    currentCol = col - 1,
    array = [],
    arrayPathCoordinates = [new Field(currentRow, currentCol, map[currentRow][currentCol])];

  let lastX = null,
    lastY = null;

  const directionX = [-1, 0, 1, 0];
  const directionY = [0, 1, 0, -1];
  let countX = 0;
  let countY = 0;

  while (!(currentRow === 0 && currentCol === 0)) {
    for (let index = currentRow - 1; index >= 0; index--) {
      if (map[index][currentCol] <= map[currentRow][currentCol]) {
        countX += 1;
      } else break;
    }

    for (let index = currentCol - 1; index >= 0; index--) {
      if (map[currentRow][index] <= map[currentRow][currentCol]) {
        countY += 1;
      } else break;
    }

    let point = null;
    if (countX > countY) {
      point = new Field(currentRow - 1, currentCol, map[currentRow][currentCol - 1]);
      array.push(point);
    } else if (countX < countY) {
      point = new Field(currentRow, currentCol - 1, map[currentRow][currentCol - 1]);
      array.push(point);
    }

    for (let index = 0; index < 4; index++) {
      let x = null, y = null;
      x = currentRow + directionX[index];
      y = currentCol + directionY[index];

      if (isInsideMap(x, y, map)) {
        if (!(x === lastX && y === lastY)) {
          point = new Field(x, y, map[x][y]);
          array.push(point);
        }
      }
    }
    countX = 0;
    countY = 0;
    array.sort(compareHeight);
    arrayPathCoordinates.push(array[0]);
    lastX = currentRow;
    lastY = currentCol;
    currentRow = array[0].x;
    currentCol = array[0].y;
    array = [];
  }

  return arrayPathCoordinates;
}

function showPathAndData(array) {

  const FUEL_ON_ONE_STEP = 1;
  let steps = array.length - 1,
    fuel = 0,
    arrayPathCoordinates = ['[0][0]'],
    stringCoordinatesPath = '';
  array = array.reverse();


  for (let index = 1; index < array.length; index++) {
    fuel += FUEL_ON_ONE_STEP + Math.abs((map[array[index - 1].x][array[index - 1].y] - map[array[index].x][array[index].y]));
    arrayPathCoordinates.push(`[${array[index].x}][${array[index].y}]`)
  }

  stringCoordinatesPath = arrayPathCoordinates.join('->');

  let pathGlobalData = `${stringCoordinatesPath}\nsteps: ${steps}\nfuel: ${fuel}`;

  return pathGlobalData;
}

function writeDataToFile(data) {
  const fs = require("fs");

  fs.writeFileSync("path-plan.txt", `${data}`, function (error) {
    if (error) throw error;
    let data = fs.readFileSync("path-plan.txt", "utf8");
  });
}

function calculateRoverPath(map) {
  const ROW = map.length;
  const COL = map[0].length;
  let vertexMatrix = createMatrixWithOptionalPath(map, ROW, COL);
  let arrayPoints = createArrayWithVertexPointsOfOptionalPath(vertexMatrix, ROW, COL);
  let data = showPathAndData(arrayPoints);
  writeDataToFile(data);
}


/* let map = [
  [0, 4],
  [1, 3]
]; */

/* let map = [
  [1, 1, 1],
  [1, 3, 1],
  [1, 3, 1]
]; */



/* let map = [
  [1, 1, 1, 3, 0],
  [1, 3, 1, 1, 5],
  [1, 2, 1, 1, 1],
  [1, 1, 1, 5, 1],
  [1, 1, 4, 7, 0],
]; */



/* let map = [
  [0, 1, 1, 1, 0],
  [1, 1, 3, 1, 1],
  [0, 1, 1, 1, 0],
  [0, 0, 0, 0, 0]
] */

/* let map = [
  [1, 1, 2, 3, 4,],
  [1, 0, 1, 2, 3,],
  [2, 1, 1, 1, 2,],
  [3, 3, 1, 0, 1,],
  [4, 3, 1, 1, 0,]
] */

/* calculateRoverPath(map); */

module.exports = {
  calculateRoverPath,
};








